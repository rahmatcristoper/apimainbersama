package com.example.apimainbareng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApimainbarengApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApimainbarengApplication.class, args);
    }

}
